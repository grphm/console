<?php
namespace STALKER_CMS\Solutions\Console\Http\Controllers;

ignore_user_abort(TRUE);
set_time_limit(0);

class Console extends ModuleController {

    private $configurations;

    public function __construct() {

        $this->configurations = config('solutions_console::config.configurations');
    }

    function getConfig($item) {

        if(!is_null($item) && isset($this->configurations[$item])):
            return $this->configurations[$item];
        else:
            return NULL;
        endif;
    }

    public function Authentication() {

        if(!empty($this->configurations['users'])):
            if(empty($_SERVER['PHP_AUTH_DIGEST'])):
                header('HTTP/1.1 401 Unauthorized');
                header('WWW-Authenticate: Digest realm="'.$this->configurations['realm'].'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($this->configurations['realm']).'"');
                die('Authenticate');
            endif;
            if(!($data = $this->httpDigestParse($_SERVER['PHP_AUTH_DIGEST'])) || !isset($this->configurations['users'][$data['username']])):
                return FALSE;
            endif;
            $A1 = md5($data['username'].':'.$this->configurations['realm'].':'.$this->configurations['users'][$data['username']]);
            $A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
            $valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);
            if($data['response'] != $valid_response):
                return FALSE;
            endif;
            return TRUE;
        endif;
    }

    public function searchCommand($userCommand, array $commands, &$found = FALSE, $inValues = TRUE) {

        foreach($commands as $key => $value):
            list($pattern, $format) = $inValues ? array($value, '$1') : array($key, $value);
            $pattern = '/^'.str_replace('\*', '(.*?)', preg_quote($pattern)).'$/i';
            if(preg_match($pattern, $userCommand)):
                if($found !== FALSE):
                    $found = preg_replace($pattern, $format, $userCommand);
                endif;
                return TRUE;
            endif;
        endforeach;
        return FALSE;
    }

    public function formatOutput($command, $output) {

        if(preg_match("%^(git )?diff%is", $command) || preg_match("%^status.*?-.*?v%is", $command)):
            $output = $this->formatDiff($output);
        endif;
        $output = $this->formatHelp($output);
        return $output;
    }

    public function executeCommand($command) {

        $descriptors = [
            0 => ["pipe", "r"], // stdin - read channel
            1 => ["pipe", "w"], // stdout - write channel
            2 => ["pipe", "w"], // stdout - error channel
            3 => ["pipe", "r"] // stdin - This is the pipe we can feed the password into
        ];
        $process = proc_open($command, $descriptors, $pipes);
        if(!is_resource($process)):
            throw new \Exception("Can't open resource with proc_open.", 500);
        endif;
        // Nothing to push to input.
        fclose($pipes[0]);
        $output = stream_get_contents($pipes[1]);
        fclose($pipes[1]);
        $error = stream_get_contents($pipes[2]);
        fclose($pipes[2]);
        // TODO: Write passphrase in pipes[3].
        fclose($pipes[3]);
        // Close all pipes before proc_close!
        $code = proc_close($process);
        return [$output, $error, $code];
    }

    private function httpDigestParse($txt) {

        $needed_parts = ['nonce' => 1, 'nc' => 1, 'cnonce' => 1, 'qop' => 1, 'username' => 1, 'uri' => 1, 'response' => 1];
        $data = [];
        $keys = implode('|', array_keys($needed_parts));
        preg_match_all('@('.$keys.')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);
        foreach($matches as $m):
            $data[$m[1]] = $m[3] ? $m[3] : $m[4];
            unset($needed_parts[$m[1]]);
        endforeach;
        return $needed_parts ? FALSE : $data;
    }

    private function formatDiff($output) {

        $lines = explode("\n", $output);
        foreach($lines as $key => $line):
            if(strpos($line, "-") === 0):
                $lines[$key] = '<span class="diff-deleted">'.$line.'</span>';
            endif;
            if(strpos($line, "+") === 0):
                $lines[$key] = '<span class="diff-added">'.$line.'</span>';
            endif;
            if(preg_match("%^@@.*?@@%is", $line)):
                $lines[$key] = '<span class="diff-sub-header">'.$line.'</span>';
            endif;
            if(preg_match("%^index\s[^.]*?\.\.\S*?\s\S*?%is", $line) || preg_match("%^diff.*?a.*?b%is", $line)):
                $lines[$key] = '<span class="diff-header">'.$line.'</span>';
            endif;
        endforeach;
        return implode("\n", $lines);
    }

    private function formatHelp($output) {

        $output = preg_replace('/_[\b](.)/is', "<u>$1</u>", $output);
        $output = preg_replace('/.[\b](.)/is', "<strong>$1</strong>", $output);
        return $output;
    }
}