<?php
namespace STALKER_CMS\Solutions\Console\Http\Controllers;

use STALKER_CMS\Vendor\Http\Controllers\Controller;

class ConsoleController extends Controller {

    private $console;

    function __construct(Console $console) {

        $this->console = $console;
        \PermissionsController::allowPermission('solutions_console', 'console');
    }

    public function index() {

        return view('solutions_console_views::index', [
                'currentDirName' => basename(base_path()),
                'currentUser' => 'developer',
                'theme' => $this->console->getConfig('theme'),
                'autocomplete' => config('solutions_console::config.autocomplete')
            ]
        );
    }

    public function execute() {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        $currentDir = base_path();
        if($request::has('command')) {
            $userCommand = urldecode($request::input('command'));
            $userCommand = escapeshellcmd($userCommand);
        } else {
            $userCommand = FALSE;
        }
        // If can - get current dir.
        if($this->console->getConfig('allowChangeDir') && isset($_GET['cd'])) {
            $newDir = urldecode($_GET['cd']);
            if(is_dir($newDir)) {
                $currentDir = $newDir;
            }
        }
        if(!$this->console->Authentication()):
            return \ResponseController::error(401)->set('errorText', "Wrong Credentials\n")->json();
        endif;
        if($userCommand !== FALSE):
            if(!empty($this->console->getConfig('allow'))):
                if(!$this->console->searchCommand($userCommand, $this->console->getConfig('allow'))):
                    $these = implode('<br>', $this->console->getConfig('allow'));
                    return \ResponseController::error(0)->set('errorText', "Sorry, but this command not allowed. Try these: $these\n")->json();
                endif;
            endif;
            // Check command by deny list.
            if(!empty($this->console->getConfig('deny'))):
                if($this->console->searchCommand($userCommand, $this->console->getConfig('deny'))):
                    return \ResponseController::error(0)->set('errorText', "Sorry, but this command is denied\n")->json();
                endif;
            endif;
            // Change current dir.
            if($this->console->getConfig('allowChangeDir') && 1 === preg_match('/^cd\s+(?<path>.+?)$/i', $userCommand, $matches)):
                $newDir = $matches['path'];
                $newDir = '/' === $newDir[0] ? $newDir : $currentDir.'/'.$newDir;
                if(is_dir($newDir) === FALSE):
                    return \ResponseController::error(0)->set('errorText', "No such directory.\n")->json();
                endif;
            endif;
            // Check if command is not in commands list.
            if(!$this->console->searchCommand($userCommand, $this->console->getConfig('commands'), $command, FALSE)):
                $these = implode('<br>', array_keys($this->console->getConfig('commands')));
                return \ResponseController::error(0)->set('errorText', "Sorry, but this command not allowed. Try these: $these\n")->json();
            endif;
            // Create final command and execute it.
            $command = "cd $currentDir && $command";
            list($output, $error, $code) = $this->console->executeCommand($command);
            $response = $this->console->formatOutput($userCommand, htmlspecialchars($output));
            return \ResponseController::success(200)->set('responseText', $response)->set('errorText', htmlspecialchars($error."\n"))->json();
        else:
            return \ResponseController::error(0)->set('errorText', "Missing command\n")->json();
        endif;
        tad($user);
    }
}