<?php

\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {
    \Route::get('console', ['as' => 'solutions.console.index', 'uses' => 'ConsoleController@index']);
    \Route::get('console/execute', ['as' => 'solutions.console.execute', 'uses' => 'ConsoleController@execute']);
});