@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('solutions_console::menu.icon') }}"></i> {!! array_translate(config('solutions_console::menu.title')) !!}
        </li>
    </ol>
@stop
@section('styles')
    {!! \Html::style('core/console/styles/'.$theme.'.css') !!}
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_console::menu.icon') }}"></i>
            {!! array_translate(config('solutions_console::menu.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="card-body card-padding p-0">
                <div id="console-container" class="scroll-overflow">
                    <p class="c-blue">
                        Console ver: {!! number_format(config('solutions_console::config.version.ver'), 1) !!}<br>
                        Version Control System Git: {!! substr(config('solutions_console::config.configurations.commands.git*'), 0, -3) !!}<br>
                        Package Manager Composer: {!! substr(config('solutions_console::config.configurations.commands.composer*'), 0, -3) !!}
                    </p>
                    <pre></pre>
                    {!! Form::open(['route' => 'solutions.console.execute', 'id' => 'console-form', 'method' => 'get']) !!}
                    <div>{!! $currentUser !!}@</div>
                    <div id="currentDirName">{!! $currentDirName !!}:~$&nbsp;</div>
                    <div id="command"><input type="text" value="" autocomplete="off"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        window.currentDir = '{!! $currentDirName !!}';
        window.currentDirName = window.currentDir.split('/').pop();
        window.currentUser = '{!! $currentUser !!}';
        window.autocomplete = {!! json_encode($autocomplete) !!};
    </script>
    {!! \Html::script('core/console/scripts/main.js') !!}
@stop