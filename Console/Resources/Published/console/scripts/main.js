$(function () {
    var maxHistory = 100;
    var position = -1;
    var currentCommand = '';
    var addCommand = function (command) {
        var ls = localStorage['commands'];
        var commands = ls ? JSON.parse(ls) : [];
        if (commands.length > maxHistory) {
            commands.shift();
        }
        commands.push(command);
        localStorage['commands'] = JSON.stringify(commands);
    };
    var getCommand = function (at) {
        var ls = localStorage['commands'];
        var commands = ls ? JSON.parse(ls) : [];
        if (at < 0) {
            position = at = -1;
            return currentCommand;
        }
        if (at >= commands.length) {
            position = at = commands.length - 1;
        }
        return commands[commands.length - at - 1];
    };
    $.fn.history = function () {
        var input = $(this);
        input.keydown(function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);

            if (code == 38) { // Up
                if (position == -1) {
                    currentCommand = input.val();
                }
                input.val(getCommand(++position));
                return false;
            } else if (code == 40) { // Down
                input.val(getCommand(--position));
                return false;
            } else {
                position = -1;
            }
        });
        return input;
    };
    $.fn.addHistory = function (command) {
        addCommand(command);
    };
});
/**
 * Autocomplete input.
 */
$(function () {
    $.fn.autocomplete = function (suggest) {
        var input = $(this);
        input.wrap('<span class="autocomplete" style="position: relative;"></span>');
        var html =
            '<span class="overflow" style="position: absolute; z-index: -10;">' +
            '<span class="repeat" style="opacity: 0;"></span>' +
            '<span class="guess"></span></span>';
        $('.autocomplete').prepend(html);
        var repeat = $('.repeat');
        var guess = $('.guess');
        var search = function (command) {
            var array = [];
            for (var key in suggest) {
                if (!suggest.hasOwnProperty(key))
                    continue;
                var pattern = new RegExp(key);
                if (command.match(pattern)) {
                    array = suggest[key];
                }
            }
            var text = command.split(' ').pop();
            var found = '';
            if (text != '') {
                for (var i = 0; i < array.length; i++) {
                    var value = array[i];
                    if (value.length > text.length &&
                        value.substring(0, text.length) == text) {
                        found = value.substring(text.length, value.length);
                        break;
                    }
                }
            }
            guess.text(found);
        };
        var update = function () {
            var command = input.val();
            repeat.text(command);
            search(command);
        };
        input.change(update);
        input.keyup(update);
        input.keypress(update);
        input.keydown(update);
        input.keydown(function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 9) {
                var val = input.val();
                input.val(val + guess.text());
                return false;
            }
        });
        return input;
    };
});
/**
 * Init console.
 */
$(function () {
    var screen = $('#console-container pre');
    var input = $('#console-container input').focus();
    var form = $('#console-form');
    var scroll = function () {
        var wtf = $('#console-container');
        var height = wtf[0].scrollHeight;
        wtf.scrollTop(height);
    };
    input.history();
    input.autocomplete(window.autocomplete);
    form.submit(function () {
        var command = $.trim(input.val());
        if (command == '') {
            return false;
        }
        $("<code>" + window.currentUser + "@" + window.currentDirName + ":~$&nbsp;" + command + "</code><br>").appendTo(screen);
        scroll();
        input.val('');
        form.hide();
        input.addHistory(command);
        $.get($(this).attr('action'), {'command': command, 'cd': window.currentDir}, function (response) {
            if (response.status) {
                screen.append(response.responseText);
            } else {
                screen.append(response.errorText);
            }
        }).fail(function () {
            screen.append("<span class='error'>Command is sent, but due to an HTTP error result is not known.</span>\n");
        }).always(function () {
            form.show();
            scroll();
        });
        return false;
    });

    $(document).keydown(function () {
        input.focus();
    });
});