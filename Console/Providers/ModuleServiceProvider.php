<?php
namespace STALKER_CMS\Solutions\Console\Providers;

use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerViews('solutions_console_views');
        $this->registerLocalization('solutions_console_lang');
        $this->registerConfig('solutions_console::config', 'Config/console.php');
        $this->registerSettings('solutions_console::settings', 'Config/settings.php');
        $this->registerActions('solutions_console::actions', 'Config/actions.php');
        $this->registerSystemMenu('solutions_console::menu', 'Config/menu.php');
        $this->publishesResources();
    }

    public function register() {
    }

    /**
     * Публикация ресурсов
     */
    public function publishesResources() {

        $this->publishes([
            __DIR__.'/../Resources/Published' => public_path('core'),
        ]);
    }
}