<?php

return [
    'console' => [
        'title' => ['ru' => 'Просмотр', 'en' => 'View', 'es' => 'Ver'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-eye'
    ]
];