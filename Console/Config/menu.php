<?php

return [
    'package' => 'solutions_console',
    'title' => ['ru' => 'Консоль', 'en' => 'Console', 'es' => 'Consola'],
    'route' => 'solutions.console.index',
    'icon' => 'zmdi zmdi-keyboard',
    'menu_child' => []
];