<?php
return [
    'package_name' => 'solutions_console',
    'package_title' => ['ru' => 'Работа с консолью', 'en' => 'Working with the console', 'es' => 'Trabajar con la consola'],
    'package_icon' => 'zmdi zmdi-keyboard',
    'relations' => [],
    'package_description' => [
        'ru' => 'Позволяет работать в консоле и выполнять команды composer и git',
        'en' => 'It allows you to work in the console and execute commands and composer git',
        'es' => 'Se le permite trabajar en la consola y ejecutar comandos git y compositor'
    ],
    'version' => [
        'ver' => 1.0,
        'date' => '13.01.2017'
    ],
    'configurations' => [
        'users' => [
            'developer' => 'WMKgolzZ1pAf'
        ],
        'realm' => 'Console',
        'allowChangeDir' => TRUE,
        'theme' => 'default', //Доступные значения: white, green, grey, far, ubuntu
        'commands' => [
            'git*' => '/usr/bin/git $1',
            'composer*' => 'HOME=/var/tmp /usr/local/bin/composer $1',
            '*' => '$1'
        ], // Список команд фильтров. Вы можете использовать * для любого символа и $1 в качестве замены.
        'allow' => [], // Список разрешенных команд
        'deny' => ['rm*'] // Список запрещенных комманд
    ],
    'autocomplete' => [
        '^\w*$' => ['cd', 'ls', 'mkdir', 'chmod', 'git', 'hg', 'diff', 'rm', 'mv', 'cp', 'more', 'grep', 'ff', 'whoami', 'kill'],
        '^git \w*$' => ['status', 'push', 'pull', 'add', 'bisect', 'branch', 'checkout', 'clone', 'commit', 'diff', 'fetch', 'grep', 'init', 'log', 'merge', 'mv', 'rebase', 'reset', 'rm', 'show', 'tag', 'remote'],
        '^git \w* .*' => ['HEAD', 'origin', 'master', 'production', 'develop', 'rename', '--cached', '--global', '--local', '--merged', '--no-merged', '--amend', '--tags', '--no-hardlinks', '--shared', '--reference', '--quiet', '--no-checkout', '--bare', '--mirror', '--origin', '--upload-pack', '--template=', '--depth', '--help']
    ]
];